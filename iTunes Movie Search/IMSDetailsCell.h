//
//  IMSDetailsCell.h
//  iTunes Movie Search
//
//  Created by Anil Sudhakaran on 4/20/14.
//  Copyright (c) 2014 Anil Sudhakaran. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface IMSDetailsCell : UITableViewCell
-(void)configureCellWithData:(NSDictionary *)data;
@property (nonatomic, copy) void(^previewBlock)(NSString *previewUrlString);
@end
