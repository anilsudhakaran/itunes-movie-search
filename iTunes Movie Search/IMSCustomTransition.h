//
//  IMSCustomTransition.h
//  iTunes Movie Search
//
//  Created by Anil Sudhakaran on 4/22/14.
//  Copyright (c) 2014 Anil Sudhakaran. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IMSCustomTransition : NSObject<UIViewControllerAnimatedTransitioning>
-(instancetype)initWithPresenting:(BOOL)presenting animationBlock:(void(^)())animationBlock;
@end
