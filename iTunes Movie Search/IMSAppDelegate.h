//
//  IMSAppDelegate.h
//  iTunes Movie Search
//
//  Created by Anil Sudhakaran on 4/19/14.
//  Copyright (c) 2014 Anil Sudhakaran. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
