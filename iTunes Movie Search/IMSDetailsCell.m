//
//  IMSDetailsCell.m
//  iTunes Movie Search
//
//  Created by Anil Sudhakaran on 4/20/14.
//  Copyright (c) 2014 Anil Sudhakaran. All rights reserved.
//

#import "IMSDetailsCell.h"

@interface IMSDetailsCell ()
@property (weak, nonatomic) IBOutlet UIImageView *artwork;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *releaseDate;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *priceHd;
@property (weak, nonatomic) IBOutlet UILabel *rating;
@property (weak, nonatomic) IBOutlet UITextView *description;
@property (weak, nonatomic) IBOutlet UIButton *preview;

@property (nonatomic, strong) NSDictionary *data;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSDateFormatter *rfc3339DateFormatter;
@property (nonatomic, strong) NSNumberFormatter *numFormatter;
@end

@implementation IMSDetailsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.dateFormatter = [[NSDateFormatter alloc] init];
        [self.dateFormatter setLocale:[NSLocale currentLocale]];
        [self.dateFormatter setDateFormat:@"MM/dd/yyyy"];
        
        self.rfc3339DateFormatter = [[NSDateFormatter alloc] init];
        NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [self.rfc3339DateFormatter setLocale:enUSPOSIXLocale];
        [self.rfc3339DateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"];
        [self.rfc3339DateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        
        self.numFormatter = [[NSNumberFormatter alloc] init];
        [self.numFormatter setLocale:[NSLocale currentLocale]];
        [self.numFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    self.title.text = nil;
    self.rating.text = nil;
    self.artwork.image = nil;
    self.price.text = nil;
    self.priceHd.text = nil;
    self.releaseDate.text = nil;
    self.description.text = nil;
    
    [self.description setTextContainerInset:UIEdgeInsetsZero];
    
    self.rating.layer.cornerRadius = 3.0;
    self.rating.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.rating.layer.borderWidth = 0.5;

    self.price.layer.cornerRadius = 3.0;
    self.price.layer.borderColor = [self.price.tintColor CGColor];
    self.price.layer.borderWidth = 0.5;

    self.priceHd.layer.cornerRadius = 3.0;
    self.priceHd.layer.borderColor = [self.price.tintColor CGColor];
    self.priceHd.layer.borderWidth = 0.5;

    [self.preview setTitle:NSLocalizedString(@"Preview", nil) forState:UIControlStateNormal];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    if (selected) {
        self.rating.layer.borderColor = [[UIColor whiteColor] CGColor];
    }
    else {
        self.rating.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    }
}

-(void)configureCellWithData:(NSDictionary *)data {
    _data = data;
    __weak IMSDetailsCell *bself = self;
    NSString *artworkURLString = data[@"artworkUrl100"];
    
    NSURLSession *urlSession = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [urlSession dataTaskWithURL:[NSURL URLWithString:artworkURLString] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        UIImage *artworkImage = [UIImage imageWithData:data scale:[[UIScreen mainScreen] scale]];
        dispatch_async(dispatch_get_main_queue(), ^{
            bself.artwork.image = artworkImage;
        });
    }];
    [task resume];
    self.title.text = data[@"trackName"];
    
    NSString *ratingText = [NSString stringWithFormat:@"%@  ",data[@"contentAdvisoryRating"]];
    self.rating.text = ratingText;
    
    NSString *priceString = [self.numFormatter stringFromNumber:data[@"collectionPrice"]];
    if (priceString) {
        self.price.text = [NSString stringWithFormat:@"%@ SD  ",priceString];
    }

    NSString *priceHdString = [self.numFormatter stringFromNumber:data[@"collectionHdPrice"]];
    if (priceHdString) {
        self.priceHd.text = [NSString stringWithFormat:@"%@ HD  ",priceHdString];
    }

    NSDate *releaseDate = [self.rfc3339DateFormatter dateFromString:data[@"releaseDate"]];
    if (releaseDate) {
        self.releaseDate.text = [self.dateFormatter stringFromDate:releaseDate];
    }
    
    self.description.text = data[@"longDescription"];
}

#pragma mark - Preview
- (IBAction)preview:(id)sender {
    NSString *previewUrlString = self.data[@"previewUrl"];
    if (previewUrlString) {
        if (self.previewBlock) {
            self.previewBlock(previewUrlString);
        }
    }
    //((UIButton *)sender).selected = !((UIButton *)sender).isSelected;
}

@end
