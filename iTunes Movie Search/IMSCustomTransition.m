//
//  IMSCustomTransition.m
//  iTunes Movie Search
//
//  Created by Anil Sudhakaran on 4/22/14.
//  Copyright (c) 2014 Anil Sudhakaran. All rights reserved.
//

#import "IMSCustomTransition.h"
@interface IMSCustomTransition ()
@property (nonatomic, assign) BOOL presenting;
@property (nonatomic, copy) void (^animationBlock)();
@end
    
@implementation IMSCustomTransition

-(instancetype)initWithPresenting:(BOOL)presenting animationBlock:(void (^)())animationBlock {
    self = [super init];
    if (self) {
        self.presenting = presenting;
        self.animationBlock = animationBlock;
    }
    return self;
}

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    return 0.5;
}

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    UIViewController *fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    CGRect fromVCFrame = [transitionContext initialFrameForViewController:fromVC];
    CGRect toVCFrame = [transitionContext finalFrameForViewController:toVC];
    CGRect finalFrame = CGRectZero;
    
    if (self.presenting) {
        CGRect initialFrame = CGRectZero;
        if (fromVC.interfaceOrientation == UIInterfaceOrientationLandscapeLeft) {
            initialFrame = CGRectMake(CGRectGetHeight(fromVCFrame), (CGRectGetHeight(fromVCFrame)-620)/2.0, 540, 620);
            finalFrame = initialFrame;
            finalFrame.origin.x = (CGRectGetWidth(fromVCFrame)-CGRectGetWidth(finalFrame))/2.0;
        }
        else if (fromVC.interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
            initialFrame = CGRectMake(-CGRectGetHeight(fromVCFrame), (CGRectGetHeight(fromVCFrame)-620)/2.0, 540, 620);
            finalFrame = initialFrame;
            finalFrame.origin.x = (CGRectGetWidth(fromVCFrame)-CGRectGetWidth(finalFrame))/2.0;
        }
        else if (fromVC.interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) {
            initialFrame = CGRectMake((CGRectGetWidth(fromVCFrame)-540)/2.0, -CGRectGetHeight(fromVCFrame), 540, 620);
            finalFrame = initialFrame;
            finalFrame.origin.y = (CGRectGetHeight(fromVCFrame)-CGRectGetHeight(finalFrame))/2.0;
        }
        else {
            initialFrame = CGRectMake((CGRectGetWidth(fromVCFrame)-540)/2.0, CGRectGetHeight(fromVCFrame), 540, 620);
            finalFrame = initialFrame;
            finalFrame.origin.y = (CGRectGetHeight(fromVCFrame)-CGRectGetHeight(finalFrame))/2.0;
        }
        toVC.view.frame = initialFrame;        
        [transitionContext.containerView addSubview:fromVC.view];
        [transitionContext.containerView addSubview:toVC.view];
    }
    else {
        if (fromVC.interfaceOrientation == UIInterfaceOrientationLandscapeLeft) {
            finalFrame = fromVCFrame;
            finalFrame.origin.x = CGRectGetHeight(toVCFrame);
        }
        else if (fromVC.interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
            finalFrame = fromVCFrame;
            finalFrame.origin.x = -CGRectGetHeight(toVCFrame);
        }
        else if (fromVC.interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) {
            finalFrame = fromVCFrame;
            finalFrame.origin.y = -CGRectGetHeight(toVCFrame);
        }
        else {
            finalFrame = fromVCFrame;
            finalFrame.origin.y = CGRectGetHeight(toVCFrame);
        }
        [transitionContext.containerView addSubview:toVC.view];
        [transitionContext.containerView addSubview:fromVC.view];
    }
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        if (self.presenting) {
            toVC.view.frame = finalFrame;
        }
        else {
            fromVC.view.frame = finalFrame;
        }
        if (self.animationBlock) {
            self.animationBlock();
        }
    } completion:^(BOOL finished) {
        [transitionContext completeTransition:YES];
    }];
    
}

@end
