//
//  IMSResultsListViewController.h
//  iTunes Movie Search
//
//  Created by Anil Sudhakaran on 4/20/14.
//  Copyright (c) 2014 Anil Sudhakaran. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString *const IMSResultsListViewControllerStoryboardID;

@interface IMSResultsListViewController : UITableViewController
-(void)configureListViewWithResults:(NSArray *)results;
@property (nonatomic, strong) UIStoryboard *listStoryboard;
@property (nonatomic, copy) void(^itemSelectionBlock)(NSDictionary *result);

@end
