//
//  IMSDetailsViewController.h
//  iTunes Movie Search
//
//  Created by Anil Sudhakaran on 4/20/14.
//  Copyright (c) 2014 Anil Sudhakaran. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString *const IMSDetailsViewControllerStoryboardID;

@interface IMSDetailsViewController : UITableViewController
-(void)configureDetailsViewWithResult:(NSDictionary *)data;
-(void)launchItunes:(id)sender;
@end
