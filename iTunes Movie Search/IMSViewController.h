//
//  IMSViewController.h
//  iTunes Movie Search
//
//  Created by Anil Sudhakaran on 4/19/14.
//  Copyright (c) 2014 Anil Sudhakaran. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString *const IMSSearchViewControllerStoryboardID;

@interface IMSViewController : UIViewController<UISearchBarDelegate,UIGestureRecognizerDelegate>
@property (nonatomic, copy) void(^searchButtonTappedBlock)(NSDictionary *result, NSString *term);
@end
