//
//  IMSContainerViewController.m
//  iTunes Movie Search
//
//  Created by Anil Sudhakaran on 4/21/14.
//  Copyright (c) 2014 Anil Sudhakaran. All rights reserved.
//

#import "IMSContainerViewController.h"
#import "IMSViewController.h"
#import "IMSResultsListViewController.h"
#import "IMSDetailsViewController.h"
#import "IMSCustomTransition.h"

@interface IMSContainerViewController ()
@property (weak, nonatomic) IBOutlet UIView *listView;
@property (weak, nonatomic) IBOutlet UIView *detailsView;
@property (nonatomic, strong) IBOutlet UIView *coverView;

@property (nonatomic, strong) NSString *storyboardName;
@property (nonatomic, strong) UIStoryboard *searchStoryboard;

@property (nonatomic, strong) IMSResultsListViewController *listVC;
@property (nonatomic, strong) IMSDetailsViewController *detailsVC;
@property (nonatomic, strong) UIBarButtonItem *buy;

@end

@implementation IMSContainerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.storyboardName = ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) ? @"Main_iPad" : @"Main_iPhone";
    
    self.searchStoryboard = [UIStoryboard storyboardWithName:self.storyboardName bundle:nil];

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(search:)];
    
    self.buy = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Buy", nil) style:UIBarButtonItemStylePlain target:self.detailsVC action:@selector(launchItunes:)];
    self.navigationItem.rightBarButtonItem = self.buy;

    self.coverView.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.4];
    self.coverView.alpha = 0.0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)disablesAutomaticKeyboardDismissal {
    return NO;
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self search:nil];
}

#pragma mark - Search Button
-(void)search:(id)sender {
    IMSViewController *searchVC = [self.searchStoryboard instantiateViewControllerWithIdentifier:IMSSearchViewControllerStoryboardID];
    searchVC.transitioningDelegate = self;
    searchVC.modalPresentationStyle = UIModalPresentationCustom;

    __weak IMSContainerViewController *bself = self;
    searchVC.searchButtonTappedBlock = ^(NSDictionary *result, NSString *term){
        
        if (!bself.listVC) {
            bself.listVC = [self.searchStoryboard instantiateViewControllerWithIdentifier:IMSResultsListViewControllerStoryboardID];
            bself.listVC.listStoryboard = bself.searchStoryboard;
            bself.listVC.itemSelectionBlock = ^(NSDictionary *result) {
                if (!bself.detailsVC) {
                    bself.detailsVC = [bself.searchStoryboard instantiateViewControllerWithIdentifier:IMSDetailsViewControllerStoryboardID];
                }
                if (![self.childViewControllers containsObject:bself.detailsVC]) {
                    [bself addChildViewController:bself.detailsVC];
                    [bself.buy setTarget:bself.detailsVC];
                    [bself.buy setAction:@selector(launchItunes:)];
                    [bself.detailsVC didMoveToParentViewController:bself];
                    bself.detailsVC.view.frame = bself.detailsView.bounds;
                    [bself.detailsView addSubview:bself.detailsVC.view];
                }
                if (result) {
                    bself.detailsVC.navigationItem.title = result[@"trackName"];
                    [bself.detailsVC configureDetailsViewWithResult:result];
                }
            };
        }
        if (term) {
            bself.listVC.navigationItem.title = [NSString stringWithFormat:@"'%@'",term];
        }

        [bself dismissViewControllerAnimated:YES completion:^{
            if (![self.childViewControllers containsObject:bself.listVC] && result) {
                [bself addChildViewController:bself.listVC];
                [bself.listVC didMoveToParentViewController:bself];
                bself.listVC.view.frame = bself.listView.bounds;
                bself.listVC.automaticallyAdjustsScrollViewInsets = NO;
                bself.listVC.edgesForExtendedLayout = UIRectEdgeNone;
                bself.listVC.extendedLayoutIncludesOpaqueBars = NO;

                [bself.listView addSubview:bself.listVC.view];
            }
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if (result) {
                    [bself.listVC configureListViewWithResults:result[@"results"]];
                }
            });

        }];
    };
    [self.navigationController presentViewController:searchVC animated:YES completion:^{
    }];
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    __weak IMSContainerViewController *bself = self;
    IMSCustomTransition *transition = [[IMSCustomTransition alloc] initWithPresenting:YES animationBlock:^{
        bself.navigationController.navigationBar.tintAdjustmentMode = UIViewTintAdjustmentModeDimmed;
        bself.coverView.alpha = 1.0;
        [bself.navigationController.navigationBar setUserInteractionEnabled:NO];
    }];
    return transition;
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    __weak IMSContainerViewController *bself = self;
    IMSCustomTransition *transition = [[IMSCustomTransition alloc] initWithPresenting:NO animationBlock:^{
        bself.navigationController.navigationBar.tintAdjustmentMode = UIViewTintAdjustmentModeNormal;
        bself.coverView.alpha = 0.0;
        [bself.navigationController.navigationBar setUserInteractionEnabled:YES];
    }];
    return transition;
}

@end
