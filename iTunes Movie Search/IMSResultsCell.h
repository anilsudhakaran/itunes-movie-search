//
//  IMSResultsCell.h
//  iTunes Movie Search
//
//  Created by Anil Sudhakaran on 4/20/14.
//  Copyright (c) 2014 Anil Sudhakaran. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMSResultsCell : UITableViewCell
-(void)configureCellWithData:(NSDictionary *)data;
@end
