//
//  IMSDetailsViewController.m
//  iTunes Movie Search
//
//  Created by Anil Sudhakaran on 4/20/14.
//  Copyright (c) 2014 Anil Sudhakaran. All rights reserved.
//

#import "IMSDetailsViewController.h"
#import "IMSDetailsCell.h"

static NSString *cellReuseIdentifier = @"detailsCell";
NSString *const IMSDetailsViewControllerStoryboardID = @"resultDetailsViewController";

@interface IMSDetailsViewController ()
@property (nonatomic, strong) NSDictionary *data;
@property (nonatomic, strong) MPMoviePlayerController *playerController;
@property (nonatomic, strong) UIBarButtonItem *buy;
@end

@implementation IMSDetailsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"IMSDetailsCell" bundle:nil] forCellReuseIdentifier:cellReuseIdentifier];

    self.buy = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Buy", nil) style:UIBarButtonItemStylePlain target:self action:@selector(launchItunes:)];
    self.navigationItem.rightBarButtonItem = self.buy;
    
    self.playerController = [[MPMoviePlayerController alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IMSDetailsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"detailsCell" forIndexPath:indexPath];
    
    // Configure the cell...
    [cell configureCellWithData:self.data];
    cell.previewBlock = ^(NSString *previewUrlString){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:previewUrlString]];
    };
    return cell;
}

-(void)configureDetailsViewWithResult:(NSDictionary *)data {
    _data = data;
    [self.tableView beginUpdates];
    [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationBottom];
    [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
    [self.tableView endUpdates];
}

#pragma mark - Launch iTunes For Purchase
-(void)launchItunes:(id)sender {
    NSString *itunesUrlString = self.data[@"collectionViewUrl"];
    if (!itunesUrlString) {
        itunesUrlString = self.data[@"trackViewUrl"];
    }
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:itunesUrlString]];
}

@end
