//
//  IMSContainerViewController.h
//  iTunes Movie Search
//
//  Created by Anil Sudhakaran on 4/21/14.
//  Copyright (c) 2014 Anil Sudhakaran. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMSContainerViewController : UIViewController<UIViewControllerTransitioningDelegate>

@end
