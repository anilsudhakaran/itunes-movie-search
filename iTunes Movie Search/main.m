//
//  main.m
//  iTunes Movie Search
//
//  Created by Anil Sudhakaran on 4/19/14.
//  Copyright (c) 2014 Anil Sudhakaran. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IMSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IMSAppDelegate class]));
    }
}
