//
//  IMSResultsListViewController.m
//  iTunes Movie Search
//
//  Created by Anil Sudhakaran on 4/20/14.
//  Copyright (c) 2014 Anil Sudhakaran. All rights reserved.
//

#import "IMSResultsListViewController.h"
#import "IMSResultsCell.h"
#import "IMSDetailsViewController.h"

static NSString *cellReuseIdentifier = @"searchResultsCell";
NSString *const IMSResultsListViewControllerStoryboardID = @"resultsListViewController";

@interface IMSResultsListViewController ()
@property (nonatomic, strong) NSArray *results;
@end

@implementation IMSResultsListViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"IMSResultsCell" bundle:nil] forCellReuseIdentifier:cellReuseIdentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.results.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IMSResultsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellReuseIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    if (self.results.count > indexPath.row) {
        [cell configureCellWithData:self.results[indexPath.row]];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.itemSelectionBlock && self.results.count > indexPath.row) {
        self.itemSelectionBlock(self.results[indexPath.row]);
    }
    else if (!self.itemSelectionBlock) {
        IMSDetailsViewController *detailsVC = [self.listStoryboard instantiateViewControllerWithIdentifier:IMSDetailsViewControllerStoryboardID];
        if (self.results.count > indexPath.row) {
            detailsVC.navigationItem.title = self.results[indexPath.row][@"trackName"];
            [detailsVC configureDetailsViewWithResult:self.results[indexPath.row]];
        }
        [self.navigationController pushViewController:detailsVC animated:YES];
    }
}

#pragma mark - Configuration Methods
-(void)configureListViewWithResults:(NSArray *)results {
    NSArray *oldIndexPaths  = [self getIndexPathsForResults:self.results];
    _results = results;
    NSArray *newIndexPaths = [self getIndexPathsForResults:self.results];
    
    [self.tableView beginUpdates];
    [self.tableView deleteRowsAtIndexPaths:oldIndexPaths withRowAnimation:UITableViewRowAnimationBottom];
    [self.tableView insertRowsAtIndexPaths:newIndexPaths withRowAnimation:UITableViewRowAnimationTop];
    [self.tableView endUpdates];
    
}

-(NSArray *)getIndexPathsForResults:(NSArray *)results {
    NSMutableArray *indexPaths  = [NSMutableArray array];
    for (id dataObject in results) {
        NSUInteger objectIndex = [results indexOfObject:dataObject];
        NSIndexPath *objectIndexPath = [NSIndexPath indexPathForRow:objectIndex inSection:0];
        if (![indexPaths containsObject:objectIndexPath]) {
            [indexPaths addObject:objectIndexPath];
        }
    }
    return indexPaths;
}

@end
