//
//  IMSViewController.m
//  iTunes Movie Search
//
//  Created by Anil Sudhakaran on 4/19/14.
//  Copyright (c) 2014 Anil Sudhakaran. All rights reserved.
//

#import "IMSViewController.h"
#import "IMSResultsListViewController.h"

static NSString *iTunesSearchBaseURL = @"https://itunes.apple.com/search?term=%@&media=movie&entity=movie&limit=200";
NSString *const IMSSearchViewControllerStoryboardID = @"searchViewController";

@interface IMSViewController ()
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UILabel *searchHelpText;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *noResultMessage;
@property (nonatomic, strong) NSString *storyboardName;
@property (nonatomic, strong) UIStoryboard *listStoryboard;
@end

@implementation IMSViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.navigationItem.title = NSLocalizedString(@"Search", nil);
    
    self.searchHelpText.text = NSLocalizedString(@"searchHelpText", nil);
    
    self.storyboardName = ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) ? @"Main_iPad" : @"Main_iPhone";
    
    self.searchBar.placeholder = NSLocalizedString(@"Search iTunes Movies", nil);
    
    self.listStoryboard = [UIStoryboard storyboardWithName:self.storyboardName bundle:nil];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.searchBar becomeFirstResponder];
    self.noResultMessage.text = nil;
}

-(BOOL)disablesAutomaticKeyboardDismissal {
    return NO;
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    if (self.presentingViewController) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
                self.view.center = self.presentingViewController.view.center;
            } completion:nil];
        });
    }
}

#pragma mark - Search Bar Delegate Methods
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    self.noResultMessage.text = nil;
    NSString *searchTerm = searchBar.text;
    [searchBar resignFirstResponder];
    if (!self.activityIndicator.isAnimating) {
        self.activityIndicator.hidden = NO;
        [self.activityIndicator startAnimating];
    }
    
    __weak IMSViewController *bself = self;
    
    NSURLSession *urlSession = [NSURLSession sharedSession];
    NSString *urlString = [[NSString stringWithFormat:iTunesSearchBaseURL, searchTerm] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURLSessionDataTask *task = [urlSession dataTaskWithURL:[NSURL URLWithString:urlString] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [bself.activityIndicator stopAnimating];
            NSString *noResultMessageText = [NSString stringWithFormat:@"No results for '%@'",searchTerm];
            if (error) {
                NSLog(@"%@",error.description);
                bself.noResultMessage.text = noResultMessageText;
            }
            else {
                NSError *jsonParsingError;
                NSDictionary *searchResult = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonParsingError];
                if (jsonParsingError) {
                    NSLog(@"%@", jsonParsingError.description);
                    bself.noResultMessage.text = noResultMessageText;
                }
                else {
                    NSUInteger count = [searchResult[@"resultCount"] integerValue];
                    if (count == 0) {
                        //Show "No results for 'search term' message;
                        bself.noResultMessage.text = noResultMessageText;
                    }
                    else {
                        if (bself.searchButtonTappedBlock) {
                            bself.searchButtonTappedBlock(searchResult, searchTerm);
                        }
                        else {
                            //Launch Next view with data;
                            IMSResultsListViewController *listVC = [self.listStoryboard instantiateViewControllerWithIdentifier:IMSResultsListViewControllerStoryboardID];
                            listVC.navigationItem.title = [NSString stringWithFormat:@"'%@'",searchTerm];
                            listVC.listStoryboard = self.listStoryboard;
                            [bself.navigationController pushViewController:listVC animated:YES];
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                [listVC configureListViewWithResults:searchResult[@"results"]];
                            });
                        }
                    }
                }
            }
        });
    }];
    [task resume];
}

#pragma mark - Tap Gesture Recognizer Action
- (IBAction)dismissKeyboard:(id)sender {
    [self.searchBar resignFirstResponder];
    if (self.searchButtonTappedBlock) {
        self.searchButtonTappedBlock(nil,nil);
    }
}

@end
