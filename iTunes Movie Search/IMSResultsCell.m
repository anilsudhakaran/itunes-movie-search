//
//  IMSResultsCell.m
//  iTunes Movie Search
//
//  Created by Anil Sudhakaran on 4/20/14.
//  Copyright (c) 2014 Anil Sudhakaran. All rights reserved.
//

#import "IMSResultsCell.h"

@interface IMSResultsCell ()
@property (weak, nonatomic) IBOutlet UIImageView *artwork;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *rating;
@property (nonatomic, strong) NSDictionary *data;
@end

@implementation IMSResultsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    self.title.text = nil;
    self.rating.text = nil;
    self.artwork.image = nil;
    self.rating.layer.cornerRadius = 2.0;
    self.rating.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.rating.layer.borderWidth = 0.5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    if (selected) {
        self.rating.layer.borderColor = [[UIColor whiteColor] CGColor];
    }
    else {
        self.rating.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    }
    // Configure the view for the selected state
}

-(void)configureCellWithData:(NSDictionary *)data {
    
    __weak IMSResultsCell *bself = self;
    NSString *artworkURLString = data[@"artworkUrl100"];
    
    NSURLSession *urlSession = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [urlSession dataTaskWithURL:[NSURL URLWithString:artworkURLString] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        UIImage *artworkImage = [UIImage imageWithData:data scale:[[UIScreen mainScreen] scale]];
        dispatch_async(dispatch_get_main_queue(), ^{
            bself.artwork.image = artworkImage;
        });
    }];
    [task resume];
    self.title.text = data[@"trackName"];
    NSString *ratingText = [NSString stringWithFormat:@"%@  ",data[@"contentAdvisoryRating"]];
    self.rating.text = ratingText;
}
@end
